import React from "react";
import {ErrorMessage, useField } from 'formik';

export const TextField = ({label, ...props}) => {
    const [field, meta] = useField(props);
    return (
        <div className="text-field">
            <label htmlFor={field.name}>{label}</label>
            <input {...field} {...props} autoComplete='off' className={`${meta.touched && meta.error && 'red'}`} />
            {/* <ErrorMessage  name={field.name}/> */}
        </div>
    )
}