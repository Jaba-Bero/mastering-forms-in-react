import React from "react";
import { Formik, Form, Field } from "formik";
import { TextField } from './textField';
import * as Yup from 'yup';

function App() {
    const validate = Yup.object({
        firstName: Yup.string().required('Required'),
        lastName: Yup.string().required('Required'),
        age: Yup.number().required('Required'),
        notes: Yup.string().max(100)
    })
  return (
    <Formik initialValues={{ firstName: "", lastName: "", age: "", employed: false, color: "", sauces: [], stooge: "Larry", notes: "" }} validationSchema={validate} onSubmit={values => {alert(JSON.stringify(values))}}>
        {formik => (
            <Form className="form">
                <TextField  label="First Name" placeholder='First Name' name='firstName' type='text' />
                <TextField  label="Last Name" placeholder='Last Name' name='lastName' type='text' />
                <TextField  label="Age" placeholder='Age' name='age' type='text' />
                <label className='employed-label' htmlFor="employed"> Employed </label>
                <Field name="employed" type="checkbox" className='employed-checkbox'/>
                <div className='colors'>
                <label   htmlFor="color">Favorite Color</label>
                <Field className="color-input" name='color' as='select'>
                    <option disabled></ option>
                    <option value='green' > Green </ option>
                    <option value='red' > Red </ option>
                    <option value='blue' > Blue </ option>
                </Field>
                </div>
                <div className="sauces-container">
                    <label className="sauces-label" htmlFor="sauces"> Sauces </label>
                    <div className="sauces">
                        <div className="input-container"><Field className='sauces-checkbox' name="sauces" type="checkbox" value="Ketchup"/> {'Ketchup'}</ div>
                        <div className="input-container"><Field className='sauces-checkbox' name="sauces" type="checkbox" value="Mustard"/> {'Mustard'}</ div>
                        <div className="input-container"><Field className='sauces-checkbox' name="sauces" type="checkbox" value="Mayonnaise"/> {'Mayonnaise'}</ div>
                        <div className="input-container"><Field className='sauces-checkbox' name="sauces" type="checkbox" value="Guacamole"/> {'Guacamole'}</ div> 
                    </div>
                </div>
                <div className="stooge-container">
                    <label htmlFor="stooge" className="stooge-label"> Best Stooge </label> 
                    <div className="stooge-inputs">
                        <div className="stooge-input-container" ><Field name="stooge" type="radio" value="Larry" className='stooge-input' /> {'Larry'}</div>
                        <div className="stooge-input-container" ><Field name="stooge" type="radio" value="Moe" className='stooge-input' />  {'Moe'}</div>
                        <div className="stooge-input-container" ><Field name="stooge" type="radio" value="Curly" className='stooge-input' />  {'Curly'}</div> 
                    </div>
                </div>
                <div className="note-container">
                    <label htmlFor="notes" className="note-label" > Notes </label>
                    <Field name='notes' type='textarea' name='notes' className='note' />
                </div>
                <button id="btn" type='submit' disabled={!formik.dirty} className="submit-btn btn">Submit</button>
                <button id="btn" type="reset" disabled={!formik.dirty} className="reset-btn btn">Reset</button>
                <pre className="pre">{JSON.stringify(formik.values, null, 2)}</pre>
            </Form>
        )}
    </Formik>
  )
}

export default App;
